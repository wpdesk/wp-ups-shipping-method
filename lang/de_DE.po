msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-10 15:04+0100\n"
"PO-Revision-Date: 2022-08-12 19:32+0200\n"
"Last-Translator: Łukasz Spryszak <lukasz.spryszak@octolize.com>\n"
"Language-Team: Łukasz Spryszak <lspryszak@gmail.com>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SearchPathExcluded-1: vendor\n"

#: src/WooCommerceShipping/Ups/Tracker.php:252
msgid "Opt-in"
msgstr "Opt-in"

#: src/WooCommerceShipping/Ups/Tracker.php:255
msgid "Opt-out"
msgstr "Opt-out"

#: src/WooCommerceShipping/Ups/UpsAdminOrderMetaDataDisplay.php:39
msgid "UPS Access Point"
msgstr "UPS Access Point"

#: src/WooCommerceShipping/Ups/UpsAdminOrderMetaDataDisplay.php:43
msgid "UPS Service Code"
msgstr "UPS Servicecode"

#: src/WooCommerceShipping/Ups/UpsAdminOrderMetaDataDisplay.php:47
msgid "Fallback reason"
msgstr "Ursache des Fallbacks"

#: src/WooCommerceShipping/Ups/UpsAdminOrderMetaDataDisplay.php:51
#: src/WooCommerceShipping/Ups/UpsFrontOrderMetaDataDisplay.php:43
msgid "UPS Access Point Address"
msgstr "UPS Access Point Adresse"

#: src/WooCommerceShipping/Ups/UpsShippingMethod.php:65
#, php-format
msgid ""
"Dynamically calculated UPS live rates based on the established UPS API "
"connection. %1$sLearn more →%2$s"
msgstr ""
"Dynamisch berechnete UPS Live-Tarife auf der Grundlage der hergestellten UPS "
"API-Verbindung. %1$sMehr erfahren →%2$s"

#: src/WooCommerceShipping/Ups/UpsSurepostShippingMethod.php:97
#, php-format
msgid ""
"Dynamically calculated UPS SurePost live rates based on the established UPS "
"API connection. %1$sLearn more →%2$s"
msgstr ""
"Dynamisch berechnete UPS SurePost Live-Tarife basierend auf der "
"hergestellten UPS API-Verbindung. %1$sMehr erfahren →%2$s"

#, php-format
#~ msgid ""
#~ "The UPS extension obtains rates dynamically from the UPS API during cart/"
#~ "checkout. %1$sRefer to the instruction manual →%2$s"
#~ msgstr ""
#~ "Wtyczka UPS wyświetla dynamicznie stawki UPS w koszyku i podsumowaniu "
#~ "zamówienia. %1$sZobacz instrukcję konfiguracji →%2$s"

#~ msgid ""
#~ "The UPS extension obtains rates dynamically from the UPS API during cart/"
#~ "checkout."
#~ msgstr ""
#~ "Metoda UPS wyświetla dynamicznie stawki UPS w koszyku i podsumowaniu "
#~ "zamówienia."
