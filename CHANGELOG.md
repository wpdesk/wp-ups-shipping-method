## [2.7.0] - 2022-08-16
### Added
- de_DE translations

## [2.6.0] - 2022-07-07
### Added
- Rates cache

## [2.5.3] - 2021-11-10
### Fixed
- SurePost method description

## [2.5.2] - 2021-11-09
### Added
- SurePost data added to tracker

## [2.5.1] - 2021-11-05
### Fixed
- SurePost settings

## [2.5.0] - 2021-11-04
### Changed
- SurePost services moved to separate shipping method

## [2.4.0] - 2021-10-28
### Changed
- SurePost services available for US domestic shipments 

## [2.3.3] - 2021-10-01
### Changed
- Method Description

## [2.3.2] - 2021-01-13
### Fixed
- Tracking for Free Shipping

## [2.3.1] - 2021-01-12
### Fixed
- Tracking for Free Shipping

## [2.3.0] - 2021-01-04
### Added
- Free Shipping Tracking
- Free Shipping Support

## [2.2.2] - 2020-04-07
### Fixed
- handled CollectionPointNotFoundException

## [2.2.1] - 2020-02-19
### Fixed
- API Status field

## [2.2.0] - 2020-01-31
### Fixed
- meta data interpreters

## [2.1.4] - 2020-01-20
### Fixed
- parameters types

## [2.1.3] - 2020-01-15
### Fixed
- utms in docs links

## [2.1.2] - 2020-01-15
### Fixed
- opt-in and opt-out links

## [2.1.1] - 2020-01-15
### Fixed
- docs link in description always visible

## [2.1.0] - 2020-01-14
### Changed
- meta data builder

## [2.0.5] - 2020-01-08
### Changed
- newest wp-woocommerce-shipping library

## [2.0.4] - 2019-12-28
### Changed
- shipping method
### Added
- hide collection point meta data in order

## [2.0.3] - 2019-12-19
### Changed
- settings fields

## [2.0.2] - 2019-12-16
### Fixed
- translations domain

## [2.0.1] - 2019-12-16
### Added
- missing translations

## [2.0.0] - 2019-12-14
### Changed
- abstract shipping v 2.0.0

## [1.0.2] - 2019-11-19
### Fixed
- admin metadata

## [1.0.1] - 2019-11-19
### Fixed
- custom origin

## [1.0.0] - 2019-11-19
### Added
- initial version
