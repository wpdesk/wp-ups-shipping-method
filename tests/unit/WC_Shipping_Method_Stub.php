<?php

class WC_Shipping_Method {

	public $instance_id = 0;

	public $settings = array();

	public $instance_settings = array();

	public function __construct( $instance_id = 0 ) {
	}

	public function init_settings() {
	}

	public function get_form_fields() {
		return array();
	}

	public function get_option( $key, $empty_value ) {
		return $empty_value;
	}

	public function init_instance_settings() {
	}

}
