<?php

use WPDesk\WooCommerceShipping\PluginShippingDecisions;
use WPDesk\WooCommerceShipping\ShopSettings;
use WPDesk\WooCommerceShipping\Ups\UpsShippingMethod;

/**
 * UPS Shipping Method Test.
 */
class UpsShippingMethodTest extends \WP_Mock\Tools\TestCase {

	/**
	 * Set up.
	 */
	public function setUp() {
		\WP_Mock::setUp();
	}

	/**
	 * Tear down.
	 */
	public function tearDown() {
		\WP_Mock::tearDown();
	}

	/**
	 * Render shipping method settings.
	 */
	public function test_admin_options() {
		\WP_Mock::userFunction( 'get_locale', array(
			'return' => 'en_US',
		) );

		\WP_Mock::userFunction( 'wp_create_nonce', array(
			'return' => 'nonce',
		) );

		\WP_Mock::userFunction( 'get_option', array(
			'return' => '',
		) );

		$shop_settings = $this->createMock( ShopSettings::class );
		$logger = new \Psr\Log\NullLogger();
		$setting_values = new \WPDesk\AbstractShipping\Settings\SettingsValuesAsArray( array() );

		$shipping_service = new \WPDesk\UpsShippingService\UpsShippingService( $logger, $shop_settings, 'PL' );

		$plugin_shipping_decisions = new PluginShippingDecisions( $shipping_service, $logger );
		$plugin_shipping_decisions->set_field_api_status_ajax( new \WPDesk\WooCommerceShipping\CustomFields\ApiStatus\FieldApiStatusAjax( $shipping_service, $setting_values, $logger ) );

		UpsShippingMethod::set_plugin_shipping_decisions( $plugin_shipping_decisions );

		$shipping_method = new UpsShippingMethod( 0 );
		$this->expectOutputRegex( '/<script/' );
		$shipping_method->admin_options();
	}

}
